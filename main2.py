class Counter:
    """
    Класс счетчик для работы с положительными числами
    """

    def __init__(self, value):
        self.value = value

    def inc(self):
        self.value += 1
        return self.value

    def dec(self):
        self.value -= 1
        return self.value


class ReverseCounter(Counter):
    """
    Класс счетчик для работы с отрицательными числами
    """

    def inc(self):
        self.value -= 1
        return self.value

    def dec(self):
        self.value += 1
        return self.value


def get_counter(number):
    """
    Метод, определяющий счетчик, который будет выбран в зависимости от входящего числа
    :param number: целое число
    :return: значение счетчика
    """
    if number >= 0:
        result_value = Counter(number)
    else:
        result_value = ReverseCounter(number)

    return result_value


counter = get_counter(-2)
a = counter.inc()
b = counter.inc()
c = counter.inc()
d = counter.dec()

print(a, b, c, d)
