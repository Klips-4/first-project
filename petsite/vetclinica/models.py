from django.db import models


class Client(models.Model):
    first_name = models.CharField('имя', max_length=30)
    last_name = models.CharField('фамилия', max_length=30)
    contact = models.CharField('контакт', max_length=255, blank=True)
    date_registration = models.DateTimeField('дата регистрации', auto_now_add=True)

    def __str__(self):
        return self.get_full_name_str()

    def get_full_name_str(self) -> str:
        last_name = self.last_name
        first_name = self.first_name

        if first_name:
            return f'{last_name} {first_name}'.strip()
        elif first_name:
            return last_name
        else:
            return 'Нет данных'


class Pet(models.Model):
    client = models.ForeignKey(Client, verbose_name='клиент', on_delete=models.PROTECT, related_name='pets')
    nickname = models.CharField('кличка', max_length=30)
    breed = models.CharField('порода', max_length=30, blank=True)
    age = models.CharField('возраст', max_length=30, blank=True)
    date_registration = models.DateTimeField('дата регистрации', auto_now_add=True)

    def __str__(self):
        return f'{str(self.client)}'


class Veterinarian(models.Model):
    service_number = models.IntegerField('табельный номер')
    contact = models.CharField('контакт', max_length=255)
    specialization = models.CharField('специализация', max_length=255)
    office = models.IntegerField('номер кабинет')

    def __str__(self):
        return self.specialization


class Services(models.Model):
    service = models.CharField('услуга', max_length=30)
    cost = models.IntegerField('стоимость', blank=True)


    def __str__(self):
        return self.service


class Reception(models.Model):
    pet = models.ForeignKey(Pet, verbose_name='клиент', on_delete=models.PROTECT, related_name='receptions')
    veterinarian = models.ForeignKey(Veterinarian, verbose_name='ветеринар', on_delete=models.PROTECT,
                                     related_name='receptions')
    date = models.CharField('дата приема', max_length=30)
    service = models.ForeignKey(Services, verbose_name='услуга', on_delete=models.PROTECT, related_name='receptions')

    def __str__(self):
        return self.date
