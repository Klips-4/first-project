from django.contrib import admin

from vetclinica.models import Client, Pet, Veterinarian, Reception, Services


@admin.register(Client)
class Client(admin.ModelAdmin):
    list_display = ('first_name', 'last_name')


@admin.register(Pet)
class Pet(admin.ModelAdmin):
    list_display = ('nickname', 'date_registration')


@admin.register(Veterinarian)
class Veterinarian(admin.ModelAdmin):
    list_display = ('service_number', 'contact', 'office')


@admin.register(Reception)
class Reception(admin.ModelAdmin):
    list = ('date')


@admin.register(Services)
class Services(admin.ModelAdmin):
    list_display = ('service', 'cost')
