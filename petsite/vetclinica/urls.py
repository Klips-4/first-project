from django.urls import path

from vetclinica.views import Mainpage, Clients, Pets, Veterinarians, Receptions

urlpatterns = [
    path('', Mainpage.as_view(), name='mainpage'),
    path('clients/', Clients.as_view(), name='clients'),
    path('pets/', Pets.as_view(), name='pets'),
    path('veterinarians/', Veterinarians.as_view(), name='veterinarians'),
    path('receptions/', Receptions.as_view(), name='receptions'),
]