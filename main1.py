# Задание №1

def what_is_the_day(dd, mm, yy):
    """
    Метод для определения переданных параметров в заданную строку
    :param dd: день
    :param mm: месяц
    :param yy: год
    :return: строка заданного формата
    """
    month_list = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября',
                  'ноября', 'декабря'
                  ]

    if (type(dd) != type(1) or dd > 31 or dd <= 0):
        return ('Ошибка, неверно введено количество дней')
    elif (type(mm) != type(1) or mm > 12 or mm <= 0):
        return ('Ошибка, неверно введено количество месяцев в году')
    elif (type(yy) != type(1) or yy <= 0 or yy > 9999):
        return ('Ошибка, неверно введен год')
    else:
        return (f'{dd} {month_list[mm - 1]} {yy} года')


# print(what_is_the_day(2, 11, 2008))


# Задание №2

def quantity_list(persons):
    """
    Метод для подсчета количества одинаковых имен в переданном кортеже строк (имен)
    :param persons: кортеж строк (имен)
    :return: словарь с именами и количеством повторений их в кортеже
    """

    return {i: persons.count(i) for i in persons}


# print(quantity_list(('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')))


# Задание №3

def person_data(data):
    """
    Метод для вывода информации о человеке в виде строки заданного формата
    :param data: полученные данные
    :return: строка c информацией заданного формата
    """
    if data.get('first_name') == None and data.get('last_name') == None:
        return ('Нет данных')
    elif data.get('first_name') == None:
        return data['last_name']
    elif data.get('last_name') == None:
        return data['first_name'] + ' ' + data['middle_name']
    elif data.get('middle_name') == None:
        return data['last_name'] + ' ' + data['first_name']
    else:
        return data['last_name'] + ' ' + data['first_name'] + ' ' + data['middle_name']


# print(person_data({'first_name': 'Иван', 'last_name': 'Иванов', 'middle_name': 'Иванович'}))


# Задание №4

def is_prime_number(number):
    """
    Метод для определения входящего числа на простоту или определить его как составное
    :param number: определяемое число
    :return: результат (True - простое число, False - составное число)
    """
    k = 0  # счетчик для подсчета количества делителей
    for i in range(2, number):
        if number % i == 0:
            k += 1

    return k <= 0


# print(is_prime_number(11))


# Задание №5

def get_uniq_numb(*args):
    """
    Метод для создания списка уникальных чисел, отсортированных в порядке возрастания
    :param args: произвольное входные параметры
    :return: отсортированный по возрастанию список уникальных чисел
    """
    unique = []  # список для заполнения уникальными числами

    for numb in args:
        if isinstance(numb, int):
            if numb not in unique:
                unique.append(numb)

    unique.sort()

    return unique

# print(get_uniq_numb(1, '2', 'text', 42, None, None, None, 15, True, 1, 1, 0, 6))
