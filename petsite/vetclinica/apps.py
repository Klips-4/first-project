from django.apps import AppConfig


class VetclinicaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vetclinica'
