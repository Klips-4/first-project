from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View

from vetclinica import models


class Mainpage(View):
    def get(self, request):
        context = {'title': 'Основная страница', 'address': 'Ufa, Street, house', 'telephone': '8-917-11-22-123'}
        return render(request=request, template_name='vetclinica/mainpage.html', context=context)


class Clients(View):
    def get(self, request):
        clients = models.Client.objects.all()
        context = {'title': 'Клиенты', 'clients': clients}
        return render(request=request, template_name='vetclinica/clients.html', context=context)


class Pets(View):
    def get(self, request):
        pets = models.Pet.objects.all()
        context = {'title': 'Питомцы', 'pets': pets}
        return render(request=request, template_name='vetclinica/pets.html', context=context)


class Veterinarians(View):
    def get(self, request):
        veterinarians = models.Veterinarian.objects.all()
        context = {'title': 'Ветеринары', 'veterinarians': veterinarians}
        return render(request=request, template_name='vetclinica/veterinarians.html', context=context)


class Receptions(View):
    def get(self, request):
        receptions = models.Reception.objects.all()
        context = {'title': 'Запись', 'receptions': receptions}
        return render(request=request, template_name='vetclinica/receptions.html', context=context)
